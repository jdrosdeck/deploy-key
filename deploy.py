import requests
import argparse
from requests.auth import HTTPBasicAuth


parser = argparse.ArgumentParser()
parser.add_argument("username", help="bitbucket username")
parser.add_argument("password", help="bitbucket password")
parser.add_argument("account", help="The name of the bitbucket account")
parser.add_argument("repo_slug", help="The repos slug")
parser.add_argument("key", help="Public key to add as deploy key")

args = parser.parse_args()


url = 'https://api.bitbucket.org/1.0/repositories/%(account_name)s/%(repo_slug)s/deploy-keys' % {'account_name':args.account,
                                                                                                 'repo_slug':args.repo_slug}

payload = {'accountname':args.account,
           'repo_slug': args.repo_slug,
           'key': args.key}

print url
r = requests.post(url, data=payload, auth=HTTPBasicAuth(args.username, args.password))

print r.text

